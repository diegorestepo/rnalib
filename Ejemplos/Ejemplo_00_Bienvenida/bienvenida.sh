#!/bin/bash

clear

echo
echo "Bienvenido"
echo

echo
echo "Usted se encuentra trabajando en el directorio:"
pwd

echo
echo "Creando nuevo directorio"
mkdir nuevo_directorio

echo
echo "Entrando al nuevo directorio"
cd nuevo_directorio

echo
echo "Ahora está en el directorio:"
pwd

echo
echo "Creando un archivo de texto"
cat > hola_mundo.txt <<EOF
Hola, esto es un mensaje de bienvenida.
Espero que la información consignada en este libro sea de utilidad.
EOF

echo
echo "Listando archivos dentro del directorio"
ls

exit 0
