#include <stdio.h>
#include <math.h>

int main(void)
{
	int i=0;
	float x=0.0;
	float y=0.0;

	FILE* salida_sin;

	salida_sin=fopen("salida_sin.dat","w");

	for(i=0;i<200;i++)
	{
		x+=0.05;
		y=sin(x);
		fprintf(salida_sin,"%5.2f %5.2f\n",x,y);
	}

	return 0;
}
