set   autoscale                        
unset log                              
unset label                            
set xtic auto                          
set ytic auto                          
set grid
plot "salida_sin.dat" u 1:2 title 'y=sin(x)' w l lw 1.5 lc rgb "red"
