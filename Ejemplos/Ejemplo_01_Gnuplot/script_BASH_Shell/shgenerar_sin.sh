#!/bin/bash

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)


GP_COMMAND=`which gnuplot 2>/dev/null`
if [ "$GP_COMMAND" = "" ];
then
         echo
         echo "gnuplot no está en el PATH"
         echo "Los resultados no pueden ser graficados"
fi

cat > generar_sin.c << EOF
#include <stdio.h>
#include <math.h>

int main(void)
{
	int i=0;
	float x=0.0;
	float y=0.0;

	FILE* salida_sin;

	salida_sin=fopen("salida_sin.dat","w");

	for(i=0;i<200;i++)
	{
		x+=0.05;
		y=sin(x);
		fprintf(salida_sin,"%5.2f %5.2f\n",x,y);
	}

	return 0;
}
EOF

echo "$AZUL Compilar $LIMPIAR"
gcc -Wall -o ejecutar generar_sin.c -lm
echo

echo "$VERDE EJECUTAR $LIMPIAR"
./ejecutar
echo

echo "$ROJO Eliminar archivos $LIMPIAR"
rm ejecutar generar_sin.c
echo

if [ "$GP_COMMAND" = "" ];
then
    break
else
cat > gnuplot.tmp <<EOF
set encoding iso_8859_15
set terminal postscript enhanced solid color "Helvetica" 20
set output "salida_sin.pdf"
#
set key off

set   autoscale
unset log
unset label
set xtic auto
set ytic auto
set grid
plot "salida_sin.dat" u 1:2 title 'y=sin(x)' w l lw 1.5 lc rgb "red"
EOF
echo "$AZUL Creando pdf $LIMPIAR"
$GP_COMMAND gnuplot.tmp
echo "$VERDE Listo $LIMPIAR"
fi

echo "$ROJO Eliminar archivos $LIMPIAR"
rm *.dat *.tmp
echo "$VERDE Listo $LIMPIAR"

exit 0
