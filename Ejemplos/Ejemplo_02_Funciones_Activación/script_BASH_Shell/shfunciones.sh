#!/bin/bash

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo "$AZUL Verificar RNAlib $LIMPIAR"
if [ -f RNAlib.h ];
then
	echo "$VERDE RNAlib.h se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.h no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.h fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.h
fi
if [ -f RNAlib.c ];
then
	echo "$VERDE RNAlib.c se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.c no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.c fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.c
fi

cat > funciones.c << EOF
#include"RNAlib.h"

int main(void)
{
	//Entrada (Estimulo de la RNA)
	float n=0.0;

	//Salida de cada una de las funciones
	float a_linear=0.0;
	float a_logsig=0.0;
	float a_tansig=0.0;

	printf("Ingrese un número decimal (estímulo de la neurona)\n");
	scanf("%f",&n);

	a_linear=linear(n);
	a_logsig=logsig(n);
	a_tansig=tansig(n);

	printf("Cuando n es %f la salida de linear es: %f\n",n,a_linear);
	printf("Cuando n es %f la salida de logsig es: %f\n",n,a_logsig);
	printf("Cuando n es %f la salida de tansig es: %f\n",n,a_tansig);

	return 0;
}
EOF

gcc -Wall -o ejecutar_shfunciones funciones.c RNAlib.c -lm
./ejecutar_shfunciones
rm ejecutar_shfunciones funciones.c

exit 0
