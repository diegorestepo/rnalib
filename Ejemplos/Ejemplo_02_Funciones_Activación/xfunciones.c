#include"RNAlib.h"

int main(void)
{
	//Entrada (Estimulo de la RNA)
	float n=0.0;

	//Salida de cada una de las funciones
	float a_linear=0.0;
	float a_logsig=0.0;
	float a_tansig=0.0;

	printf("Ingrese un número decimal (estímulo de la neurona)\n");
	scanf("%f",&n);

	a_linear=linear(n);
	a_logsig=logsig(n);
	a_tansig=tansig(n);

	printf("Cuando n es %f la salida de linear es: %f\n",n,a_linear);
	printf("Cuando n es %f la salida de logsig es: %f\n",n,a_logsig);
	printf("Cuando n es %f la salida de tansig es: %f\n",n,a_tansig);

	return 0;
}

