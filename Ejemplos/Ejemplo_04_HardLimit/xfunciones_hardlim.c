#include"RNAlib.h"

int main(void)
{
	//Entrada (Estimulo de la RNA)
	float n=0.0;

	//Salida de cada una de las funciones
	int a_hardlim=0.0;
	int a_hardlims=0.0;

	printf("Ingrese un número decimal (estímulo de la neurona)\n");
	scanf("%f",&n);

	a_hardlim=hardlim(n);
	a_hardlims=hardlims(n);

	printf("Cuando n es %f la salida de linear es: %d\n",n,a_hardlim);
	printf("Cuando n es %f la salida de logsig es: %d\n",n,a_hardlims);

	return 0;
}
