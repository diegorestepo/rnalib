#!/bin/bash

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo "$AZUL Verificar RNAlib $LIMPIAR"
if [ -f RNAlib.h ];
then
	echo "$VERDE RNAlib.h se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.h no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.h fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.h
fi
if [ -f RNAlib.c ];
then
	echo "$VERDE RNAlib.c se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.c no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.c fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.c
fi

cat > rnaor.c << EOF
#include"RNAlib.h"

int main(void)
{
	//Entradas
	float P1=0.0, P2=0.0;

	//Pesos sinapticos
	float W1=1.0, W2=1.0;

	//Bias
	float b=-1.0;

	//Salida
	float a=0;

	printf("\nIngrese las entradas P1 y P2\n");
	scanf("%f",&P1);
	scanf("%f",&P2);

        a = neurona_2in_1out(P1,P2,W1,W2,b,hardlim);

	printf("\nCuando P es [%f %f],\n",P1,P2);
	printf("la salida es: %d\n\n",(int)a);

	return 0;
}
EOF

gcc -Wall -o ejecutaror rnaor.c RNAlib.c -lm
./ejecutaror
rm ejecutaror rnaor.c

exit 0
