#include"RNAlib.h"

int main(void)
{
	//Entradas
	float P1=0.0, P2=0.0;

	//Pesos sinapticos
	float W1=1.0, W2=1.0;

	//Bias
	float b=-1.0;

	//Salida
	float a=0;

	printf("\nIngrese las entradas P1 y P2\n");
	scanf("%f",&P1);
	scanf("%f",&P2);

        a = neurona_2in_1out(P1,P2,W1,W2,b,hardlim);

	printf("\nCuando P es [%f %f],\n",P1,P2);
	printf("la salida es: %d\n\n",(int)a);

	return 0;
}
