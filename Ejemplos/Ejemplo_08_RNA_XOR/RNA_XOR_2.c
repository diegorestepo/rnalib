#include"RNAlib.h"

int main(void)
{
    float P1, P2;

    float W1[2][2]={{8.4204,8.0895},{-9.2747,-9.6075}};
    float** mW1;
    float b1[2]={-12.5021,4.1393};
    float* vb1;

    float W2[2]={-19.5154,-23.3266};
    float* vW2;
    float b2 = 11.1044;

    float P[]={};
    float* vP;

    float a1[]={};
    float* va1;

    float a2=0.0;

    int i=0;

    vP = vector(1,2);
    vP = P;

    mW1=matriz(1,2,1,2);
    for(i=0;i<2;i++)
    {
        mW1[i] = W1[i];
    }

    vb1 = vector(1,2);
    vb1 = b1;

    va1 = vector(1,2);
    va1 = a1;

    vW2 = vector(1,2);
    vW2 = W2;

    printf("====================\n");
    printf("=     RNA XOR      =\n");
    printf("====================\n");
    printf("Ingrese las entradas de la RNA, deben ser 0 o 1\n");

    printf("Ingrese la primera entrada (P1):");
    scanf("%f",&P1);

    printf("Ingrese la segunda entrada (P2):");
    scanf("%f",&P2);

    vP[0] = P1;
    vP[1] = P2;

    //Comprobación de entradas.
    if((P1 == 1 || P1 == 0) && (P2 == 1 || P2 == 0))
    {
        //Capa1:
        capa(vP,mW1,vb1,2,logsig,va1);

        //Capa2:
        a2 = neurona(va1,vW2,b2,2,logsig);

        printf("\nLa salida de la RNA es: %f\n",round(a2));
        printf("Cuando las entradas son P1=%f y P2=%f\n",P[0],P[1]);
    }
    else
    {
        printf("Entradas no validas\n");
    }

    return 0;
}
