#!/bin/bash

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo "$AZUL Verificar RNAlib $LIMPIAR"
if [ -f RNAlib.h ];
then
	echo "$VERDE RNAlib.h se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.h no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.h fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.h
fi
if [ -f RNAlib.c ];
then
	echo "$VERDE RNAlib.c se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.c no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.c fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.c
fi

cat > rnaxor.c << EOF
#include"RNAlib.h"

int main(void)
{
	/* ========== Entradas ========== */
	float P1, P2;

	/* ========== Capa1 ========== */
	//Neurona1
	float W1_11=8.4204;
	float W2_11=8.0895;
	float b_11=-12.5021;
	//Neurona2
	float W1_21=-9.2747;
	float W2_21=-9.6075;
	float b_21=4.1393;

	/* ========== Capa2 ========== */
	//Neurona
	float W1_12=-19.5154;
	float W2_12=-23.3266;
	float b_12=11.1044;

	/* ========== Salidas ========== */
	float a1_1=0.0, a2_1=0.0, a=0.0;

	printf("====================\n");
	printf("=     RNA XOR      =\n");
	printf("====================\n");
	printf("Ingrese las entradas de la RNA, deben ser 0 o 1\n");

	printf("Ingrese la primera entrada (P1):");
	scanf("%f",&P1);

	printf("Ingrese la segunda entrada (P2):");
	scanf("%f",&P2);

	//Comprobación de entradas.
	if((P1 == 1 || P1 == 0) && (P2 == 1 || P2 == 0))
	{
		//Capa1:
		a1_1 = neurona_2in_1out(P1,P2,W1_11,W2_11,b_11,logsig); //Estimulo 1 de la capa 1.
		a2_1 = neurona_2in_1out(P1,P2,W1_21,W2_21,b_21,logsig); //Estimulo 2 de la capa 1.

		//Capa2:
		a = neurona_2in_1out(a1_1,a2_1,W1_12,W2_12,b_12,logsig);

		printf("\nLa salida de la RNA es: %f\n",round(a));
		printf("Cuando las entradas son P1=%f y P2=%f\n",P1,P2);
	}
	else
	{
		printf("Entradas no validas\n");
	}

	return 0;
}
EOF

gcc -Wall -o ejecutarxor rnaxor.c RNAlib.c -lm
./ejecutarxor
rm ejecutarxor rnaxor.c

exit 0
