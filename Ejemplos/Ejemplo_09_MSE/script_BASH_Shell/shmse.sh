#!/bin/bash

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo "$AZUL Verificar RNAlib $LIMPIAR"
if [ -f RNAlib.h ];
then
	echo "$VERDE RNAlib.h se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.h no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.h fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.h
fi
if [ -f RNAlib.c ];
then
	echo "$VERDE RNAlib.c se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.c no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.c fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.c
fi

cat > mse.c << EOF
#include"RNAlib.h"

int main(void)
{
	float T[4]={0,1,1,0};
	float arna[4]={0.0000,0.9999,0.9999,0.0003};
	float ax[4]={3.1000,0.5720,1.5001,2.403};
	float erna=0.0, ex=0.0;

	erna = mse(&T[0],&arna[0],4);
	ex   = mse(&T[0],&ax[0],4);

	printf("erna = %2.10f\n",erna);
	printf("ex = %f\n",ex);

	return 0;
}
EOF

gcc -Wall -o ejecutarmse mse.c RNAlib.c -lm
./ejecutarmse
rm ejecutarmse mse.c

exit 0
