#include"RNAlib.h"

int main(void)
{
	float T[4]={0,1,1,0};
	float arna[4]={0.0000,0.9999,0.9999,0.0003};
	float ax[4]={3.1000,0.5720,1.5001,2.403};
	float erna=0.0, ex=0.0;

	erna = mse(&T[0],&arna[0],4);
	ex   = mse(&T[0],&ax[0],4);

	printf("erna = %f\n",erna);
	printf("ex = %f\n",ex);

	return 0;
}
