#!/bin/bash

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)


GP_COMMAND=`which gnuplot 2>/dev/null`
if [ "$GP_COMMAND" = "" ];
then
         echo
         echo "gnuplot no está en el PATH"
         echo "Los resultados no pueden ser graficados"
fi

echo "$AZUL Verificar RNAlib $LIMPIAR"
if [ -f RNAlib.h ];
then
	echo "$VERDE RNAlib.h se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.h no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.h fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.h
fi
if [ -f RNAlib.c ];
then
	echo "$VERDE RNAlib.c se encuentra en el directorio $LIMPIAR"
	echo
else
	echo "$ROJO RNAlib.c no encuentra en el directorio $LIMPIAR"
	echo "$AZUL RNAlib.c fue llamado al directorio $LIMPIAR"
	echo
	ln -s ../../../librerias/RNAlib.c
fi

cat > normalizar.c << EOF
#include"RNAlib.h"

float polinonio(float x)
{
	return pow(x,2) - 4;
}

int main(void)
{
	int i=0;
	float x=-5, xmin = -5, xmax = 5;
	float y=0.0, ymin=-4, ymax=21;
	float xn=0.0, yn=0.0;

	FILE *normalizar;

	normalizar=fopen("normalizar.dat","w");

	for(i=0;i<21;i++)
	{
		y  = polinonio(x);

		//Normalización
                xn = normalizar_1_1(x,xmax,xmin,0);
                yn = normalizar_1_1(y,ymax,ymin,0);

		//Generación de archivo
		fprintf(normalizar,"%f  %f  %f  %f\n",x,y,xn,yn);
		x+=0.5;
	}

	return 0;
}
EOF

echo "$AZUL Compilar $LIMPIAR"
gcc -Wall -o ejecutar normalizar.c RNAlib.c -lm
echo

echo "$VERDE EJECUTAR $LIMPIAR"
./ejecutar
echo

echo "$ROJO Eliminar archivos $LIMPIAR"
rm ejecutar normalizar.c
echo

if [ "$GP_COMMAND" = "" ];
then
    break
else
cat > gnuplot.tmp <<EOF
set encoding iso_8859_15
set terminal postscript enhanced solid color "Helvetica" 20
set output "valores.pdf"
#
set key off

set   autoscale
unset log
unset label
set xtic auto
set ytic auto
set grid
plot "normalizar.dat" u 1:2 title 'Valores originales' w l lw 1.5 lc rgb "red"
EOF
echo "$AZUL Creando pdf $LIMPIAR"
$GP_COMMAND gnuplot.tmp
echo "$VERDE Listo $LIMPIAR"
fi

if [ "$GP_COMMAND" = "" ];
then
    break
else
cat > gnuplot.tmp <<EOF
set encoding iso_8859_15
set terminal postscript enhanced solid color "Helvetica" 20
set output "valoresnormalizados.pdf"
#
set key off

set   autoscale
unset log
unset label
set xtic auto
set ytic auto
set grid
plot "normalizar.dat" u 3:4 title 'Valores normalizados' w l lw 1.5 lc rgb "red"
EOF
echo "$AZUL Creando pdf $LIMPIAR"
$GP_COMMAND gnuplot.tmp
echo "$VERDE Listo $LIMPIAR"
fi

echo "$ROJO Eliminar archivos $LIMPIAR"
rm *.dat *.tmp
echo "$VERDE Listo $LIMPIAR"

exit 0
