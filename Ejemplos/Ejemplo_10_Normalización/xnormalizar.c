#include"RNAlib.h"

float polinonio(float x)
{
	return pow(x,2) - 4;
}

int main(void)
{
	int i=0;
	float x=-5, xmin = -5, xmax = 5;
	float y=0.0, ymin=-4, ymax=21;
	float xn=0.0, yn=0.0;
	float xd=0.0, yd=0.0;

	FILE *normalizar;

	normalizar=fopen("normalizar.dat","w");

	for(i=0;i<21;i++)
	{
		y  = polinonio(x);

		//Normalización
		xn = normalizar_1_1(x,xmax,xmin,0);
        yn = normalizar_1_1(y,ymax,ymin,0);

		//Normalización inversa
        xd = normalizar_1_1(xn,xmax,xmin,1);
        yd = normalizar_1_1(yn,ymax,ymin,1);

		//Generación de archivo
//		fprintf(normalizar,"%f  %f  %f  %f  %f  %f\n",x,y,xn,yn,xd,yd);
		fprintf(normalizar,"%f  %f  %f  %f\n",x,y,xn,yn);
		x+=0.5;
	}

	return 0;
}
