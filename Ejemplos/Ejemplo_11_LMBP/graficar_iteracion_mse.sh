#!/bin/bash

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

GP_COMMAND=`which gnuplot 2>/dev/null`
if [ "$GP_COMMAND" = "" ];
then
         echo
         echo "gnuplot no está en el PATH"
         echo "Los resultados no pueden ser graficados"
fi

if [ "$GP_COMMAND" = "" ];
then
    break
else
cat > gnuplot.tmp <<EOF
set encoding iso_8859_15
set terminal postscript enhanced solid color "Helvetica" 20
set output "epocas.pdf"
#

set   autoscale
unset log
unset label
set xtic auto
set ytic auto
set xlabel "Iteraciones"
set ylabel "Error Medio Cuadrado"
set grid
plot "epocas.dat" u 1:2 title 'Epocas' w l lw 4 lc rgb "red"
EOF
echo "$AZUL Creando pdf $LIMPIAR"
$GP_COMMAND gnuplot.tmp
echo "$VERDE Listo $LIMPIAR"
fi

exit 0
