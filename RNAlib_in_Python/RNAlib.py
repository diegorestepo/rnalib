'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''

import numpy as np


def normalizar_array(func):
    def inner(x,sw):
        xmax = np.max(x)
        xmin = np.min(x)

        return func(x,xmax,xmin,sw)

    return inner

def normalizar(x, xmax, xmin, sw):

    if xmax == xmin:
        y = x
    elif sw == 0:
        y = 2*(x - xmin)/(xmax - xmin) - 1
    elif sw == 1:
        y = 0.5*(x + 1)*(xmax - xmin) + xmin

    return y


def error(T, a):
    return T - a


def mse(T, a):
    e = np.power(error(T, a), 2)
    return e/len(T)


class Neurona:
    def __init__(self, P, W, b):
        self.P = P
        self.W = W
        self.b = b

    def estimulo(self):
        self.n = np.dot(self.W, self.P) + self.b
        return self.n

    def hardlim(self):
        if self.n >= 0:
            return 1
        else:
            return 0

    def hardlims(self):
        if self.n >= 0:
            return 1
        else:
            return -1

    def linear(self):
        return self.n

    def logsig(self):
        return 1/(1 + np.exp(-self.n))

    def tansig(self):
        return 2/(1 + np.exp(-2*self.n)) - 1

    def devlinear(self):
        return 1

    def devlogsig(self):
        return (1 - Neurona.logsig(self))*Neurona.logsig(self)

    def devtansig(self):
        return 1 - (Neurona.tansig(self)*Neurona.tansig(self))


class Capa(Neurona):
    def __init__(self, P, W, b, function):
        super().__init__(P, W, b)

        self.function = function

    def activar(self):
        self.n = Neurona.estimulo(self)

        if self.function == "hardlim":
            self.a = Neurona.hardlim(self)
        elif self.function == "hardlims":
            self.a = Neurona.hardlims(self)
        elif self.function == "linear":
            self.a = Neurona.linear(self)
        elif self.function == "logsig":
            self.a = Neurona.logsig(self)
        elif self.function == "tansig":
            self.a = Neurona.tansig(self)

        return self.a


class FeedforWard:
    def __init__(self, P1, W1, b1, function1, W2, b2, function2):
        self.P1 = P1
        self.W1 = W1
        self.b1 = b1
        self.function1 = function1

        self.W2 = W2
        self.b2 = b2
        self.function2 = function2

    def MLP(self):
        capa1 = Capa(self.P1, self.W1, self.b1, self.function1)
        capa2 = Capa(self.P1, self.W2, self.b2, self.function2)

        self.a1 = capa1.activar()
        capa2.P = self.a1

        self.a2 = capa2.activar()
        return(self.a2)
