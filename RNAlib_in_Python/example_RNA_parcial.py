'''
 * ------------------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
 * ------------------------------------------------------------------------------------
'''

import matplotlib.pyplot as plt
import numpy as np
from RNAlib import *


def _parcial0(P):
    W1 = np.array([[-32.518608093261719],
                   [14.175943374633789],
                   [28.429965972900391],
                   [-31.991678237915039],
                   [-31.945470809936523],
                   [-40.443000793457031],
                   [30.188728332519531],
                   [-26.74560546875],
                   [24.506956100463867],
                   [-23.419382095336914],
                   [17.690526962280273],
                   [-28.719329833984375],
                   [30.832267761230469],
                   [38.451042175292969],
                   [12.547882080078125],
                   [17.479129791259766],
                   [19.082359313964844],
                   [-24.966842651367188],
                   [-38.222011566162109],
                   [-27.308462142944336],
                   [30.251409530639648],
                   [-31.756677627563477],
                   [33.168128967285156],
                   [-33.886917114257812],
                   [34.433780670166016]])

    b1 = np.array([[37.511791229248047],
                   [-13.918296813964844],
                   [-35.886440277099609],
                   [25.832477569580078],
                   [23.705095291137695],
                   [25.112331390380859],
                   [-15.547930717468262],
                   [10.456954956054687],
                   [-7.5850644111633301],
                   [12.217339515686035],
                   [-3.7657794952392578],
                   [2.5014913082122803],
                   [-0.16496677696704865],
                   [1.0763480663299561],
                   [2.3227815628051758],
                   [5.3007321357727051],
                   [7.7086114883422852],
                   [-13.032770156860352],
                   [-20.191848754882813],
                   [-26.322296142578125],
                   [21.5762939453125],
                   [-20.372720718383789],
                   [25.985605239868164],
                   [-28.820615768432617],
                   [32.984363555908203]])

    W2 = np.array([[-0.27321168780326843, -0.30719897150993347, 0.26939594745635986, 0.23050020635128021, 0.15556102991104126, 1.1368868350982666, -2.0836308002471924, -0.14743803441524506, 0.11530257761478424, -3.2022285461425781, 0.14182601869106293, -0.049937967211008072,
                    0.073014728724956512, -0.065093949437141418, 0.10926997661590576, 0.076529256999492645, 0.085781954228878021, -0.31839591264724731, 0.31223848462104797, -3.1288409233093262, 0.084293574094772339, -0.064613856375217438, 0.093679614365100861, -0.11753692477941513, -2.0736267566680908]])

    b2 = np.array([-1.2043198347091675])

    rna = FeedforWard(P1=P, W1=W1, b1=b1, function1="tansig",
                      W2=W2, b2=b2, function2="tansig")

    a = rna.MLP()

    return a


def _parcial1(P):
    W1 = np.array([[-32.215923309326172],
                   [33.352703094482422],
                   [28.928728103637695],
                   [-31.713821411132812],
                   [-31.792240142822266],
                   [-32.832389831542969],
                   [32.044589996337891],
                   [-28.937656402587891],
                   [35.289730072021484],
                   [-24.733102798461914],
                   [25.316263198852539],
                   [-39.706062316894531],
                   [36.079952239990234],
                   [27.29498291015625],
                   [27.939411163330078],
                   [28.127252578735352],
                   [28.491851806640625],
                   [-33.276214599609375],
                   [-36.168262481689453],
                   [-19.125946044921875],
                   [27.506555557250977],
                   [-27.992973327636719],
                   [30.63697624206543],
                   [-30.845134735107422],
                   [33.832435607910156]])

    b1 = np.array([[29.523237228393555],
                   [-33.135025024414063],
                   [-26.528203964233398],
                   [24.170148849487305],
                   [20.760515213012695],
                   [17.570535659790039],
                   [-16.250301361083984],
                   [11.36590576171875],
                   [-11.393980979919434],
                   [12.965754508972168],
                   [-2.5879342555999756],
                   [2.1018259525299072],
                   [-0.37242847681045532],
                   [3.1785359382629395],
                   [5.6404237747192383],
                   [8.0159378051757813],
                   [10.461475372314453],
                   [-15.923154830932617],
                   [-17.4033203125],
                   [-11.822434425354004],
                   [24.191751480102539],
                   [-19.748193740844727],
                   [23.818460464477539],
                   [-30.549131393432617],
                   [29.707292556762695]])

    W2 = np.array([[-1.0561238527297974, -0.2908015251159668, -1.3250877857208252, 1.0612517595291138, -0.6109699010848999, 1.4334288835525513, -1.1010756492614746, -0.10375747829675674, 0.087778598070144653, -2.9624612331390381, 0.070670224726200104, 0.12195189297199249, -
                    0.030654981732368469, 0.051135919988155365, 0.059856787323951721, 0.061243440955877304, 0.062606632709503174, -1.2573550939559937, 1.3428118228912354, -0.12699130177497864, 1.0934401750564575, -0.091206997632980347, 0.10091124475002289, -0.99660569429397583, -0.77898555994033813]])

    b2 = np.array([-1.9275563955307007])

    rna = FeedforWard(P1=P, W1=W1, b1=b1, function1="tansig",
                      W2=W2, b2=b2, function2="tansig")

    a = rna.MLP()

    return a


def _parcial2(P):
    W1 = np.array([[34.184959411621094],
                   [32.390941619873047],
                   [-31.521055221557617],
                   [37.342117309570313],
                   [37.919677734375],
                   [-33.355388641357422],
                   [-32.699790954589844],
                   [32.85107421875],
                   [24.686485290527344],
                   [24.034675598144531],
                   [-22.891208648681641],
                   [21.752107620239258],
                   [21.754871368408203],
                   [-41.241222381591797],
                   [39.560935974121094],
                   [-30.718204498291016],
                   [-24.158596038818359],
                   [21.230953216552734],
                   [22.397472381591797],
                   [23.797462463378906],
                   [25.154207229614258],
                   [-26.472745895385742],
                   [27.722953796386719],
                   [28.911893844604492],
                   [31.170520782470703]])

    b1 = np.array([[-35.916416168212891],
                   [-31.225381851196289],
                   [28.051713943481445],
                   [-24.952362060546875],
                   [-25.220920562744141],
                   [20.201921463012695],
                   [18.050806045532227],
                   [-16.164100646972656],
                   [-7.9137172698974609],
                   [-5.3241262435913086],
                   [2.8038859367370605],
                   [-0.44981235265731812],
                   [1.8461148738861084],
                   [-7.3310379981994629],
                   [7.1738786697387695],
                   [-8.0042266845703125],
                   [-8.0184783935546875],
                   [9.2256221771240234],
                   [11.787322044372559],
                   [14.534855842590332],
                   [17.372478485107422],
                   [-20.303388595581055],
                   [23.301412582397461],
                   [26.388523101806641],
                   [31.101547241210938]])

    W2 = np.array([[0.62980693578720093, -0.14692971110343933, 0.052114255726337433, 1.5361746549606323, -1.7376670837402344, 0.19036856293678284, 0.13688001036643982, -0.082710638642311096, 0.077012859284877777, 0.084287993609905243, -0.080868668854236603, 0.077956371009349823,
                    0.079425044357776642, -2.4718444347381592, -2.8411145210266113, 1.2110178470611572, -1.0204625129699707, 0.27203911542892456, 0.16059048473834991, 0.12786650657653809, 0.11834235489368439, -0.12309110164642334, 0.1464570164680481, 0.22186613082885742, 1.2683603763580322]])

    b2 = np.array([-1.592406153678894])

    rna = FeedforWard(P1=P, W1=W1, b1=b1, function1="tansig",
                      W2=W2, b2=b2, function2="tansig")

    a = rna.MLP()

    return a


def _parcial3(P):
    W1 = np.array([[-36.233085632324219],
                   [34.174140930175781],
                   [34.094047546386719],
                   [-37.943405151367188],
                   [36.456516265869141],
                   [-34.997791290283203],
                   [31.828969955444336],
                   [34.350395202636719],
                   [-19.39179801940918],
                   [46.802818298339844],
                   [30.124094009399414],
                   [28.194589614868164],
                   [27.692205429077148],
                   [-27.44195556640625],
                   [-27.404481887817383],
                   [-27.563705444335938],
                   [27.886724472045898],
                   [28.342739105224609],
                   [-28.892217636108398],
                   [29.500608444213867],
                   [30.143070220947266],
                   [30.794059753417969],
                   [31.438194274902344],
                   [32.068515777587891],
                   [33.114185333251953]])

    b1 = np.array([[34.135089874267578],
                   [-32.105056762695313],
                   [-29.12725830078125],
                   [25.596288681030273],
                   [-24.445486068725586],
                   [18.739986419677734],
                   [-19.274961471557617],
                   [-18.39729118347168],
                   [5.1559514999389648],
                   [-16.677862167358398],
                   [-3.8963537216186523],
                   [-1.0910820960998535],
                   [1.2512562274932861],
                   [-3.500493049621582],
                   [-5.7379040718078613],
                   [-8.0109643936157227],
                   [10.348738670349121],
                   [12.769289970397949],
                   [-15.276815414428711],
                   [17.867664337158203],
                   [20.535770416259766],
                   [23.269346237182617],
                   [26.06207275390625],
                   [28.929035186767578],
                   [32.600017547607422]])

    W2 = np.array([[-0.9438776969909668, -1.0247312784194946, -0.040720906108617783, -1.2994885444641113, -1.497689962387085, -2.4759063720703125, -0.12804868817329407, -2.5728282928466797, 1.0476791858673096, -0.27054485678672791, 0.64273166656494141, 0.22603800892829895,
                    0.14178316295146942, -0.10712761431932449, -0.089153394103050232, -0.079040378332138062, 0.073502391576766968, 0.071153797209262848, -0.071553237736225128, 0.074899390339851379, 0.082175292074680328, 0.095990829169750214, 0.12360872328281403, 0.19408348202705383, 0.83655780553817749]])

    b2 = np.array([-1.9876605272293091])

    rna = FeedforWard(P1=P, W1=W1, b1=b1, function1="tansig",
                      W2=W2, b2=b2, function2="tansig")

    a = rna.MLP()

    return a


def _entrada():
    parcial = int(input("Ingrese 0, 1, 2, 3:\n"))

    if parcial == 0:
        Vmax = 23.2904
        Pmax = 194.8649
        Pmin = 0.1407
        finc = 2329
    elif parcial == 1:
        Vmax = 21.400394
        Pmax = 97.500023
        Pmin = 0.107924
        finc = 2140
    elif parcial == 2:
        Vmax = 13.720219
        Pmax = 31.866497
        Pmin = 0.071195
        finc = 1372
    elif parcial == 3:
        Vmax = 13.720219
        Pmax = 44.414711
        Pmin = 0.058594
        finc = 1372

    return Vmax, Pmax, Pmin, finc, parcial


def run():
    step = 0.01
    V = step
    Vmin = 0.01

    Vmax, Pmax, Pmin, finc, parcial = _entrada()

    x = []
    y = []

    for i in range(finc):
        V1 = normalizar(V, Vmax, Vmin, 0)

        if parcial == 0:
            a = _parcial0(V1)
        elif parcial == 1:
            a = _parcial1(V1)
        elif parcial == 2:
            a = _parcial2(V1)
        elif parcial == 3:
            a = _parcial3(V1)

        a1 = normalizar(a, Pmax, Pmin, 1)

        x.append(V)
        y.append(a1.reshape(-1))

        V = V + step

    plt.plot(x, y)
    plt.show()


if __name__ == '__main__':
    run()
