#include"RNAlib.h"

/* ========== Normalización de [-1, 1] ========== */
//sw=0 :: Normalizar.
//sw=1 :: Revertir normalización.
float normalizar_1_1(float x, float xmax, float xmin, int sw)
{
	float y=0.0;

	if(xmax==xmin)
	{
		y=x;
	}
	else if(sw == 0)
	{
		y=2*(x-xmin)/(xmax-xmin)-1;
	}
	else
	{
		y=0.5*(x+1)*(xmax-xmin)+xmin;
	}

	return y;
}

/* ========== Función de activación ========== */
//Función de activación hardlim
float hardlim(float n)
{
	float a=0;

	if(n>=0)
		a=1;
	else
		a=0;

	return a;
}

//Función de activación hardlims
float hardlims(float n)
{
	float a=0;

	if(n>=0)
		a=1;
	else
		a=-1;

	return a;
}

//Función de activación linear
float linear(float n)
{
	return n;
}

//Función de activación logsig
float logsig(float n)
{
	return 1/(1+exp(-n));
}

//Función de activación tansig
float tansig(float n)
{
	return 2/(1 + exp(-2*n)) - 1;
}

//Derivadas de funciones de activación
float devlinear(float n)
{
	return 1;
}

float devlogsig(float n)
{
	return (1-logsig(n))*logsig(n);	
}

float devtansig(float n)
{
	return 1 - (tansig(n)*tansig(n));
}

/* ================== Error ================= */
float error(float T, float a)
{
	return (T - a);
}

/* ========== Error Medio Cuadrado ========== */
float mse(float *T, float *a, int cont)
{
	int i=0;
	float e=0.0;

	for(i=0;i<cont;i++)
	{
		e += pow(T[i]-a[i],2);
	}

	return e/cont;
}

/* ========== Neurona ========== */
//Neurona una entrada y una salida.
float neurona_1in_1out(float P1, float W1, float b, float (*f)(float))
{
	float n=0.0, a=0.0;

	n = W1*P1 + b;
	a = (*f)(n);

	return a;
}

//Neurona de dos entradas y una salida.
float neurona_2in_1out(float P1, float P2, float W1, float W2, float b, float (*f)(float))
{
	float n=0.0, a=0.0;

	n = W1*P1 + W2*P2 + b;
	a = (*f)(n);

	return a;
}

//Neurona de cont entradas y una salida.
float neurona(float *P, float *W, float b, int cont, float (*f)(float))
{
	int i=0;
	float PW=0.0, n=0.0, a=0.0;

	for(i=0;i<cont;i++)
	{
		PW += P[i]*W[i];
	}

	n = PW + b;
	a = (*f)(n);

	return a;
}

//Capa de neuronas de cont entradas y const salidas.
void capa(float *P, float **W, float *b, int cont, float (*f)(float), float *a)
{
	int i=0;
	float n[]={};
	float* vn;

	vn = vector(1,cont);
	vn = n;

	multiplicarmatrizvector(W,cont,cont,P,vn);

	for(i=0;i<cont;i++)
	{
		n[i] += b[i];
		a[i] = (*f)(n[i]);
	}
} 

/* ================== Vectores y Matrices ================= */
//Asignar vector flotante (Tomado de Numerical Recipes <nrutil.c>)
float *vector(int nl, int nh)
{
	float *v;

	v = (float *)malloc((size_t)((nh-nl+1+NR_END)*sizeof(float)));
	return v-nl+NR_END;
}	

//Asignar matriz flotante (Tomado de Numerical Recipes <nrutil.c>)
float **matriz(int fl, int fh, int cl, int ch)
{
	long i, nf=fh-fl+1, nc=ch-cl+1;
	float **m;

	//Asignar filas
	m = (float **)malloc((size_t)((nf+NR_END)*sizeof(float*)));
	m += NR_END;
	m -= fl;

	//Asignar filas y establecer punteros
	m[fl] = (float *)malloc((size_t)((nf*nc+NR_END)*sizeof(float)));
	m[fl] += NR_END;
	m[fl] -= fl;

	for(i=fl+1; i<=fh;i++)
	{
		m[i] = m[i-1]+nc;
	}

	return m;
} 

//sw=0 :: imprimir como vector columna.
//sw=1 :: imprimir como vector fila.
void imprimirvector(float *V, int val, int sw)
{
	int i=0;

	for(i=0;i<val;i++)
	{
		if(sw==0)
			printf(" %f\n",V[i]);
		else
			printf(" %f  ",V[i]);
	}
}

//Sumar dos vectores.
void sumarvectores(float *V1, float *V2, int val, float *resultado)
{
	int i=0;

	for(i=0;i<val;i++)
	{
		resultado[i]=V1[i]+V2[i];
	}
}

//Invertir signo de los elementos del vector.
void invertirsignovector(float *V1, int val)
{
	int i=0;

	for(i=0;i<val;i++)
	{
		V1[i] = -V1[i];
	}
}

//Función para imprimir matrices.
void imprimirmatriz(float **M, int fil, int col)
{	
	int i=0, j=0;

	for (i=0;i<fil;i++)
	{
		for (j=0;j<col;j++)
		{
			printf(" %f ",M[i][j]);
		}
		
		printf("\n");

	}
}

//Matriz con elemento en la diagonal principal, ingrese el valor '1' para obtener la matriz identidad.
void matrizdiagonal(float **M, int val, float factor)
{
	int i=0, j=0;

	for(i=0;i<val;i++)
	{
		for(j=0;j<val;j++)
		{
			M[i][j]=(i==j)*factor;
		}
	}
}

//Sumar dos matrices.
void sumarmatrices(float **M1, float **M2, int fil, int col, float **resultado)
{
	int i=0, j=0;

	for(i=0;i<fil;i++)
	{
		for(j=0;j<col;j++)
		{	
			resultado[i][j]=M1[i][j]+M2[i][j];
		}
	}
}

//Multiplicar dos matrices.
void multiplicarmatrices(float **M1, float **M2, int vali, int valk, int valj, float **resultado)
{
	int i=0, j=0, k=0, aux=0;

	for(i=0;i<vali;i++)
	{
		for(k=0;k<valk;k++)
		{
			aux=0.0;
			for(j=0;j<valj;j++)
			{
				aux+=M1[i][j]*M2[j][k];
				resultado[i][k]=aux;
			}
		}
	}
}

//Trasponer una matriz.
void matriztranspuesta(float **M1, float **M2, int fil, int col)
{
	int i=0, j=0;

	for (i=0;i<col;i++)
	{
		for (j=0;j<fil;j++)
		{
			M2[i][j]=M1[j][i];
		}
	}
}

//Multiplicar una matriz por un vector.
void multiplicarmatrizvector(float **M1, int fil, int col, float *V1, float *resultado)
{
	int i=0, j=0;
	float aux=0.0;

	for (i=0;i<fil;i++)
	{
		aux=0.0;
		for (j=0;j<col;j++)
		{
			aux+=M1[i][j]*V1[j];
			resultado[i]=aux;
		}
	}
}

/* ================== Gauss-Jordan ================= */
//Concatena la matriz de entrada con la matriz identidad.
void matrizinicial(float **M1, float **MI, int fil, int col, float **M2)
{
	int i=0, j=0;

	for(i=0;i<fil;i++)
	{
		for(j=0;j<col;j++)
		{
			M2[i][j] = M1[i][j];
			M2[i][j+col] = MI[i][j];
		}
	}
}

//Convierte elementos de la diagonal principal a 1.
void paso1(float **M1, int fil, int col, int e1, int e2, float **M2)
{
	int i=0, j=0;
	float c=0.0;

	if(M1[e1][e2] == -1)
	{
		for(j=0;j<col;j++)
		{
			M1[e1][j] = -M1[e1][j];
		}
	}
	else if(M1[e1][e2] != 1)
	{
		c=M1[e1][e2];

		for(j=0;j<col;j++)
		{
			M1[e1][j] = M1[e1][j]/c;
		}
	}

	for(i=0;i<fil;i++)
	{
		for(j=0;j<col;j++)
		{
			M2[i][j] = M1[i][j];
		}
	}

}

//Convierte elementos que no son de la diagonal principal a 0.
void paso2(float **M1, int fil, int col, int e1, int e2, float **M2)
{
	int i=0, j=0;
	float c=0.0;

	if(M1[e1][e2] != 0)
	{
		c = -M1[e1][e2];

		for(j=0;j<col;j++)
		{
			M1[e1][j] += c*M1[e2][j];
		}

	}

	for(i=0;i<fil;i++)
	{
		for(j=0;j<col;j++)
		{
			M2[i][j] = M1[i][j];
		}
	}

}

//Imprimir matriz inversa.
void matrizinversa(float **M1, int fil, int col, float **M2)
{
	int i=0, j=0;

	for(i=0;i<fil;i++)
	{
		for(j=0;j<col;j++)
		{
			M2[i][j] = M1[i][j+col/2];
		}
	}
}

//Método de eliminación de Gauss-Jordan para hallar la inversa de una matriz.
void gaussjordan(float **I, float **M1, int fil, float **M2)
{
	int i=0, j=0, k=0;	

	matrizdiagonal(I,fil,1);
	matrizinicial(M1,I,fil,fil,M2);

	for(k=0;k<fil;k++)
	{
		for(j=0;j<fil;j++)
		{
			if(j==k)
			{
				paso1(M2,fil,2*fil,j,j,M2);
				for(i=0;i<fil;i++)
				{
					if(i!=k)
					{
						paso2(M2,fil,2*fil,i,j,M2);
					}
				}
			}
		}
	}

	matrizinversa(M2,fil,2*fil,M2);
}

/* ==================       Jacobiano     ================= */
//J = Y*X
//Convertir Sensitive a matriz Y para luego multiplicarlo por X
void sensitive2Y(float **mS,int fcuentk, int fcuenj, int fcuenti, float **my)
{
	int i=0, j=0, k=0;

	for(k=0;k<fcuentk;k++)
	{
		for(j=0;j<fcuenj;j++)
		{
			for(i=0;i<fcuenti;i++)
			{
				my[j][i+k*fcuentk]=mS[k][j];
			}
		}
	}
}

/* ================== Levenberg-Marquardt ================= */
// Función Levenberg-Marquardt.
void func_lm(float **mJ, float *ve, float u, float *vdx)
{
	int i=0;

	float Jt[col_lm][fil_lm]={};
	float **mJt;

	float JtJ[col_lm][col_lm]={};
	float **mJtJ;

	float I[col_lm][col_lm]={};
	float **mI;

	float GJ[col_lm][col_lm*2]={};
	float **mGJ;

	float JtV[col_lm]={};
	float *vJtV; 

	vJtV = vector(1,col_lm);
	vJtV = JtV;

	mJt  = matriz(1,col_lm,1,fil_lm);
	mJtJ = matriz(1,col_lm,1,col_lm);
	mI   = matriz(1,col_lm,1,col_lm);
	mGJ = matriz(1,col_lm,1,col_lm*2);

	for(i=0;i<col_lm;i++)
	{
		mJt[i]=Jt[i];
		mJtJ[i]=JtJ[i];
		mI[i]=I[i];
		mGJ[i]=GJ[i];
	}

	matriztranspuesta(mJ,mJt,fil_lm,col_lm);
	multiplicarmatrices(mJt,mJ,col_lm,col_lm,fil_lm,mJtJ);
	matrizdiagonal(mI,col_lm,u);
	sumarmatrices(mJtJ,mI,col_lm,col_lm,mJtJ);
	gaussjordan(mI,mJtJ,col_lm,mGJ);
	multiplicarmatrizvector(mJt,col_lm,fil_lm,ve,vJtV);
	multiplicarmatrizvector(mGJ,col_lm,col_lm,vJtV,vdx);

	free(mJ);
	mJ=NULL;

	free(mJt);
	mJt=NULL;

	free(mJtJ);
	mJt=NULL;

	free(mI);
	mI=NULL;

	free(mGJ);
	mGJ=NULL;
} 
