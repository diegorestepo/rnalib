#ifndef RNALIB
#define RNALIB

#include<stdio.h>
#include<stddef.h>
#include<stdlib.h>
#include<math.h>
#define NR_END 1
#define fil_lm 2
#define col_lm 4

/* ========== Normalización de [-1, 1] ========== */
float normalizar_1_1(float x, float xmax, float xmin, int sw);

/* ========== Función de activación ========== */
float hardlim(float n);
float hardlims(float n);
float linear(float n);
float logsig(float n);
float tansig(float n);

// Derivadas de las funciones de activación.
float devlinear(float n);
float devlogsig(float n);
float devtansig(float n);

/* ================== Error ================= */
float error(float T, float a);

// Error Medio Cuadrado
float mse(float *T, float *a, int cont);

/* ========== Neurona ========== */
float neurona_1in_1out(float P1, float W1, float b, float (*f)(float));
float neurona_2in_1out(float P1, float P2, float W1, float W2, float b, float (*f)(float));
float neurona(float *P, float *W, float b, int cont, float (*f)(float));
void capa(float *P, float **W, float *b, int cont, float (*f)(float), float *a);

/* ================== Algebra líneal ================= */
float *vector(int nl, int nh);
float **matriz(int fl, int fh, int cl, int ch);
void imprimirvector(float *V, int val, int sw);
void sumarvectores(float *V1, float *V2, int val, float *resultado);
void invertirsignovector(float *V1, int val);
void imprimirmatriz(float **M, int fil, int col);
void matrizdiagonal(float **M, int val, float factor);
void sumarmatrices(float **M1, float **M2, int fil, int col, float **resultado);
void multiplicarmatrices(float **M1, float **M2, int vali, int valk, int valj, float **resultado);
void matriztranspuesta(float **M1, float **M2, int fil, int col);
void multiplicarmatrizvector(float **M1, int fil, int col, float *V1, float *resultado);
void matrizinicial(float **M1, float **MI, int fil, int col, float **M2);
void paso1(float **M1, int fil, int col, int e1, int e2, float **M2);
void paso2(float **M1, int fil, int col, int e1, int e2, float **M2);
void matrizinversa(float **M1, int fil, int col, float **M2);
void gaussjordan(float **I, float **M1, int fil, float **M2);
void sensitive2Y(float **mS,int fcuentk, int fcuenj, int fcuenti, float **my);
void func_lm(float **mJ, float *ve, float u, float *vdx);

#endif
